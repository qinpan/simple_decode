
/* CRC余式表 */
static const unsigned short crc_ta[16] =
{
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
};

unsigned short crc16(unsigned short init_crc, void *buff, int len)
{
    unsigned short crc = init_crc;
    unsigned char *ptr = buff;
    unsigned char da;

    while (len-- > 0)
    {
        da = crc >> 12;                 /* 暂存CRC的高四位 */
        crc <<= 4;                      /* CRC左移4位，相当于取CRC的低12位 */
        crc ^= crc_ta[da^(*ptr>>4)];    /* CRC的高4位和本字节的前半字节相加后查表 */
        
        da = crc >> 12;                 /* 暂存CRC的高4位 */
        crc <<= 4;                      /* CRC左移4位，相当于CRC的低12位 */
        crc ^= crc_ta[da^(*ptr&0x0f)];  /* CRC的高4位和本字节的后半字节相加后查表 */

        ptr++;
    }

    return crc;
}

