/*******************
  TEA加密解密算法
*******************/

#include "btea.h"

/* 
*key  maybe 128bit =16 Bytes.
*buf  maybe BLOCK_SIZE
*/

void btea_encrypt(unsigned char* buf, unsigned char* key)
{
    unsigned char n=BTEA_BLOCK_SIZE/4;
    unsigned int *v=(unsigned int *)buf;
    unsigned int *k=(unsigned int *)key;
    unsigned int z = v[n - 1],y = v[0],sum = 0,e ;
    unsigned char p, q;
    // Coding Part 
    
    q = BTEA_S_LOOPTIME + 52 / n ;
    while ( q-- > 0 )
    {
        sum += BTEA_DELTA ;
        e = sum >> 2 & 3 ;
        for ( p = 0 ; p < n - 1 ; p++ )
        {
          y = v[p + 1],
          z = v[p] += BTEA_MX;
          y = v[0] ;
        }
        z = v[n - 1] += BTEA_MX;
    }
}

/*
*key  maybe 128bit =16Bytes.
*buf  maybe BLOCK_SIZE
inbuf == outbuf == buf
*/

void btea_decrpyt(unsigned char* buf, unsigned char* key)
{
    unsigned char n=BTEA_BLOCK_SIZE/4;
    unsigned int *v=(unsigned int *)buf;
    unsigned int *k=(unsigned int *)key;
    unsigned int z = v[n - 1],y = v[0],sum = 0,e ;
    unsigned char  p,q ;
    
    //Decoding Part...
    q = BTEA_S_LOOPTIME + 52 / n ;
    sum = q * BTEA_DELTA ;
    while ( sum != 0 )
    {
        e = sum >> 2 & 3 ;
        for ( p = n - 1 ; p > 0 ; p-- )
        {
            z = v[p - 1],
            y = v[p] -= BTEA_MX;
            z = v[n - 1] ;
        }
        y = v[0] -= BTEA_MX;
        sum -= BTEA_DELTA ;
    }
}

