#include <stdlib.h>
#include "simple_decode.h"
#include "crc.h"

enum decode_state_e {
    STATE_0 = 0,        /* s0  未找到帧头 */
    STATE_10,           /* s10 已找到帧头，接收正常数据 */
    STATE_11,           /* s11 已找到帧头，接收转义数据 */
};

static enum decode_state_e decode_state = STATE_0;
static int decode_count = 0;
static unsigned char decode_buff[SIMPLE_DECODE_BUFF_SIZE];
static void (*decode_handler)(unsigned char *buff, int len) = NULL;

void set_decode_handler(void (*handler)(unsigned char *buff, int len))
{
    decode_handler = handler;
}

static void may_do_handler(void)
{
    unsigned short check1;
    unsigned short check2;
    
    if (decode_count < 3 || decode_state != STATE_10)
        return;

    check1 = crc16(0, decode_buff, decode_count - 2);
    check2 = decode_buff[decode_count - 2] | (decode_buff[decode_count - 1] << 8);
    
    if (decode_handler && check1 == check2)
        decode_handler(decode_buff, decode_count - 2);
}

#define simple_decode_find_head() do{\
    decode_count = 0;\
    decode_state = STATE_10;\
}while(0)

#define check_decode_count_and_inc(ch) do{\
    if (decode_count >= SIMPLE_DECODE_BUFF_SIZE)\
    {\
        decode_state = STATE_0;\
        return;\
    }\
    decode_buff[decode_count++] = (ch);\
    decode_state = STATE_10;\
}while(0)

void simple_decode(unsigned char ch)
{
    if (ch == SIMPLE_DECODE_MAGIC_1)            /* 找到帧头 */
    {
        may_do_handler();
        simple_decode_find_head();
        return;
    }

    if (decode_state == STATE_10)               /* s10 已找到帧头，接收正常数据 */
    {
        if (ch == SIMPLE_DECODE_MAGIC_2)        /* 找到转义数据 */
            decode_state = STATE_11;
        else                                    /* 找到正常数据 */
            check_decode_count_and_inc(ch);
    }
    else if(decode_state == STATE_11)           /* s11 已找到帧头，接收转义数据 */
    {
        if (ch == SIMPLE_DECODE_MAGIC_3)        /* 转义数据 1 */
            check_decode_count_and_inc(SIMPLE_DECODE_MAGIC_1);
        else if (ch == SIMPLE_DECODE_MAGIC_4)   /* 转义数据 2 */
            check_decode_count_and_inc(SIMPLE_DECODE_MAGIC_2);
        else 
            decode_state = STATE_0;             /* 异常数据 */
    }
}

#define simple_encode_ch(ch) do{\
    if (out_length > max_outlength - 3)\
        return -1;\
    if ((ch) == SIMPLE_DECODE_MAGIC_1)\
    {\
        outbuff[out_length++] = SIMPLE_DECODE_MAGIC_2;\
        outbuff[out_length++] = SIMPLE_DECODE_MAGIC_3;\
    }\
    else if ((ch) == SIMPLE_DECODE_MAGIC_2)\
    {\
        outbuff[out_length++] = SIMPLE_DECODE_MAGIC_2;\
        outbuff[out_length++] = SIMPLE_DECODE_MAGIC_4;\
    }\
    else\
    {\
        outbuff[out_length++] = (ch);\
    }\
}while(0)

int simple_encode(unsigned char *inbuff, int in_length, unsigned char *outbuff, int max_outlength)
{
    int i;
    int out_length = 0;
    unsigned short check = crc16(0, inbuff, in_length);

    outbuff[out_length++] = SIMPLE_DECODE_MAGIC_1;
    
    for (i = 0; i < in_length; i++)
        simple_encode_ch(inbuff[i]);

    simple_encode_ch(check & 0xff);
    simple_encode_ch(check >> 8);

    outbuff[out_length++] = SIMPLE_DECODE_MAGIC_1;
    return out_length;
}

