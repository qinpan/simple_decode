#ifndef SIMPLE_DECODE_H_
#define SIMPLE_DECODE_H_

/* 解码缓冲区大小 */
#define SIMPLE_DECODE_BUFF_SIZE (1024)

/* 转义 f5 -> f6 c3, f6 -> f6 c4 */
#define SIMPLE_DECODE_MAGIC_1 (0xf5)
#define SIMPLE_DECODE_MAGIC_2 (0xf6)
#define SIMPLE_DECODE_MAGIC_3 (0xc3)
#define SIMPLE_DECODE_MAGIC_4 (0xc4)

int simple_encode(unsigned char *inbuff, int in_length, unsigned char *outbuff, int max_outlength);
void simple_decode(unsigned char ch);
void set_decode_handler(void (*handler)(unsigned char *buff, int len));

#endif // SIMPLE_DECODE_H_

