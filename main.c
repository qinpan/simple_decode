#include <stdio.h>
#include <string.h>
#include "simple_decode.h"
#include "crc.h"
#include "btea.h"

//TEA密钥
static unsigned char TEA_key[16]=
{ 
    0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,
    0x09,0x0A,0x0B,0xCC,0x0D,0x0E,0x0F,0x10
};

static void dump_decode_data(unsigned char *buff, int len)
{
    int i = 0;

    for (i = 0; i < len / BTEA_BLOCK_SIZE; i++)
        btea_decrpyt(buff + i * BTEA_BLOCK_SIZE, TEA_key);

    printf("cmd len:%d", len);
    for (i = 0; i < len; i++)
    {        
        if (i % 16 == 0)
            printf("\n%08x: ", i);
        else if (i % 8 == 0)
            printf(" ");
        
        printf("%02x ", (unsigned int)buff[i]);
    }

    printf("\n");
}

int main(int argc, char **argv)
{
    int i = 0;
    int count = 0;
    int test_inbuff_count;
    unsigned char test_inbuff[2048];
    unsigned char test_outbuff[2048 * 2];

    set_decode_handler(dump_decode_data);

    printf("test1\n");
    test_inbuff_count = 10;
    test_inbuff_count = (test_inbuff_count + BTEA_BLOCK_SIZE - 1) & ~(BTEA_BLOCK_SIZE - 1);
    for (i = 0; i < test_inbuff_count; i++)
        test_inbuff[i] = i;
    for (i = 0; i < test_inbuff_count / BTEA_BLOCK_SIZE; i++)
        btea_encrypt(test_inbuff + i * BTEA_BLOCK_SIZE, TEA_key);
    count = simple_encode(test_inbuff, test_inbuff_count, test_outbuff, 2048);
    for (i = 0; i < count; i++)
        simple_decode(test_outbuff[i]);

    printf("test2\n");
    test_inbuff_count = SIMPLE_DECODE_BUFF_SIZE - 8;
    test_inbuff_count = (test_inbuff_count + BTEA_BLOCK_SIZE - 1) & ~(BTEA_BLOCK_SIZE - 1);
    for (i = 0; i < test_inbuff_count; i++)
        test_inbuff[i] = i;
    for (i = 0; i < test_inbuff_count / BTEA_BLOCK_SIZE; i++)
        btea_encrypt(test_inbuff + i * BTEA_BLOCK_SIZE, TEA_key);
    count = simple_encode(test_inbuff, test_inbuff_count, test_outbuff, 2048);
#if 0
    test_outbuff[100] = SIMPLE_DECODE_MAGIC_1;      /* 构造错误数据 */
#endif
    for (i = 0; i < count; i++)
        simple_decode(test_outbuff[i]);

    printf("test3\n");
    test_inbuff_count = SIMPLE_DECODE_BUFF_SIZE - 8;
    test_inbuff_count = (test_inbuff_count + BTEA_BLOCK_SIZE - 1) & ~(BTEA_BLOCK_SIZE - 1);
    for (i = 0; i < test_inbuff_count; i++)
        test_inbuff[i] = i;
    for (i = 0; i < test_inbuff_count / BTEA_BLOCK_SIZE; i++)
        btea_encrypt(test_inbuff + i * BTEA_BLOCK_SIZE, TEA_key);
    count = simple_encode(test_inbuff, test_inbuff_count, test_outbuff, 2048);
    for (i = 0; i < count; i++)
    {
#if 0
        if (i == 100)                               /* 构造丢包场景 */
            continue;
#endif
        simple_decode(test_outbuff[i]);
    }

    printf("test4\n");                              /* 编码缓冲区边界测试 */
    test_inbuff_count = 10;
    test_inbuff_count = (test_inbuff_count + BTEA_BLOCK_SIZE - 1) & ~(BTEA_BLOCK_SIZE - 1);
    for (i = 0; i < test_inbuff_count; i++)
        test_inbuff[i] = i;
    for (i = 0; i < test_inbuff_count / BTEA_BLOCK_SIZE; i++)
        btea_encrypt(test_inbuff + i * BTEA_BLOCK_SIZE, TEA_key);
    count = simple_encode(test_inbuff, test_inbuff_count, 
                        test_outbuff, test_inbuff_count + 5);
    for (i = 0; i < count; i++)
        simple_decode(test_outbuff[i]);

    printf("test5\n");                              /* 解码缓冲区边界测试 */
    test_inbuff_count = SIMPLE_DECODE_BUFF_SIZE - 8;
    test_inbuff_count = (test_inbuff_count + BTEA_BLOCK_SIZE - 1) & ~(BTEA_BLOCK_SIZE - 1);
    for (i = 0; i < test_inbuff_count; i++)
        test_inbuff[i] = i;
    for (i = 0; i < test_inbuff_count / BTEA_BLOCK_SIZE; i++)
        btea_encrypt(test_inbuff + i * BTEA_BLOCK_SIZE, TEA_key);
    count = simple_encode(test_inbuff, test_inbuff_count, test_outbuff, 2048);
    for (i = 0; i < count; i++)
        simple_decode(test_outbuff[i]);

    return 0;
}

