#ifndef __TEA_h__
#define __TEA_h__

#define BTEA_MX                ((z>>5)^(y<<2))+(((y>>3)^(z<<4))^(sum^y))+(k[(p&3)^e]^z)
#define BTEA_DELTA             0x9e3779b9
#define BTEA_S_LOOPTIME        1     // 5
#define BTEA_BLOCK_SIZE        8     // 压缩算法以块为单位进行压缩(单位:字节)

//TEA加密函数
void btea_encrypt(unsigned char* buf, unsigned char* key);

//TEA解密函数
void btea_decrpyt(unsigned char* buf, unsigned char* key);

#endif

